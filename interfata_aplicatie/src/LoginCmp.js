import React from "react"

class LoginCmp extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			username: "",
			password: ""
		}

		this.onChange = this.onChange.bind(this)
		this.submit = this.submit.bind(this)
	}

	onChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	submit() {
		this.props.submit(this.state.username, this.state.password).then(data => {
			if (data) {
				this.props.history.push("/principal")
			}
		})
	}

	render() {
		return (
			<div className='col-sm-3'>
				Autentificare
				<br></br>
				<br></br>
				<form>
					<input
						value={this.state.username}
						placeholder='username'
						className='form-control'
						type='text'
						name='username'
						onChange={this.onChange}
					/>
					<input
						value={this.state.password}
						placeholder='password'
						className='form-control mb-2'
						type='password'
						name='password'
						onChange={this.onChange}
					/>
					<button type='button' className='btn btn-sm btn-primary' onClick={this.submit}>
						Login
					</button>
				</form>
			</div>
		)
	}
}

export default LoginCmp

import React from "react"
import ActivitateCmp from "./ActivitateCmp"
import { Link } from "react-router-dom"
import axios from "axios"

class PrincipalCmp extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			activitati: [],
			legaturi: []
		}

		this.refresh = this.refresh.bind(this)
	}
	async refresh() {
		await axios
			.get("http://18.191.219.161:8080/api/legaturaActivitateUtilizator/" + this.props.utilizator.id)
			.then(data => {
				this.setState({ legaturi: data.data })
			})
	}
	componentDidMount() {
		this.refresh()
		axios.get("http://18.191.219.161:8080/api/activitate").then(data => {
			this.setState({ activitati: data.data })
		})
	}

	render() {
		return (
			<div className='ml-3 mt-1'>
				<h3>Lista activitati</h3>
				<div className='row'>
					{this.state.activitati.map(activitate => (
						<ActivitateCmp
							utilizator={this.props.utilizator}
							activitate={activitate}
							legaturi={this.state.legaturi}
							refresh={this.refresh}
						></ActivitateCmp>
					))}
				</div>
				{this.props.utilizator.profesor ? (
					<Link to='/activitateNoua'>
						<button type='button' className='btn mt-3 btn-sm btn-primary'>
							Adauga
						</button>
					</Link>
				) : null}
			</div>
		)
	}
}

export default PrincipalCmp

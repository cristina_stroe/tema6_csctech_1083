import React from "react"
import axios from "axios"

let setIntervalResponse

class FeedbackCmp extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			emoticons: [],
			activitate: {},
			legaturi: []
		}

		this.realizeazaLegatura = this.realizeazaLegatura.bind(this)
	}

	async realizeazaLegatura(emoticon) {
		await axios.post("http://18.191.219.161:8080/api/legaturaActivitateEmoticon", {
			idActivitate: this.props.match.params.id,
			idEmoticon: emoticon.id,
			data: new Date()
		})
	}

	async componentDidMount() {
		const self = this
		if (this.props.utilizator.profesor) {
			setIntervalResponse = setInterval(() => {
				axios
					.get("http://18.191.219.161:8080/api/legaturaActivitateEmoticon/" + self.props.match.params.id)
					.then(data => {
						this.setState({ legaturi: data.data })
					})
			}, 500)
		}
		await axios
			.get("http://18.191.219.161:8080/api/activitate/" + self.props.match.params.id)
			.then(activitate => {
				this.setState({ activitate: activitate.data })
			})
		await axios.get("http://18.191.219.161:8080/api/emoticons").then(data => {
			this.setState({ emoticons: data.data })
		})
	}

	componentWillUnmount = () => {
		if (this.props.utilizator.profesor) {
			clearInterval(setIntervalResponse)
		}
	}

	render() {
		return (
			<div className='mt-2 ml-1'>
				<h3>
					Nume: {this.state.activitate.numeActivitate} Durata: {this.state.activitate.durata} minute
				</h3>
				<h4 className='mb-5'>{new Date(this.state.activitate.data).toLocaleDateString()}</h4>
				{!this.props.utilizator.profesor
					? this.state.emoticons.map(emoticon => (
							<div
								className='card bg-info ml-2 mb-3'
								style={{ width: 80 + "%" }}
								onClick={() => this.realizeazaLegatura(emoticon)}
							>
								<div className='card-body mr-2 mx-auto'>{emoticon.emoticon}</div>
							</div>
					  ))
					: this.state.emoticons.map(emoticon => (
							<div
								className='card bg-info ml-2 mb-3'
								style={{ width: 40 + "%" }}
								onClick={() => this.realizeazaLegatura(emoticon)}
							>
								<div className='card-body mr-2 mx-auto'>
									{emoticon.emoticon} ->{" "}
									{
										this.state.legaturi.filter(legatura => legatura.idEmoticon === emoticon.id)
											.length
									}
								</div>
							</div>
					  ))}
			</div>
		)
	}
}

export default FeedbackCmp

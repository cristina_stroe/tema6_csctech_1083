import React from "react"
import axios from "axios"
import DateTimePicker from "react-datetime-picker"

class ActivitateNouaCmp extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			numeActivitate: "",
			about: "",
			durata: "",
			data: new Date()
		}
		this.onChange = this.onChange.bind(this)
		this.onChangeData = this.onChange.bind(this)
		this.submit = this.submit.bind(this)
	}
	onChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	onChangeData(data) {
		this.setState({ data: new Date(data) })
	}

	async submit() {
		await axios.post("http://18.191.219.161:8080/api/activitate", {
			numeActivitate: this.state.numeActivitate,
			durata: this.state.durata,
			data: this.state.data,
			idUtilizator: this.props.utilizator.id
		})

		this.props.history.push("/principal")
	}
	render() {
		return (
			<div className='col-sm-4'>
				<form>
					<label>Nume</label>
					<input
						value={this.state.numeActivitate}
						className='form-control'
						type='text'
						name='numeActivitate'
						onChange={this.onChange}
					/>
					<label>Durata</label>
					<input
						value={this.state.durata}
						className='form-control mb-2'
						type='number'
						name='durata'
						onChange={this.onChange}
					/>
					<div className='row ml-1'>
						<DateTimePicker
							name='data'
							onChange={this.onChangeData}
							value={this.state.data}
						/>
					</div>
					<button type='button' className='mt-2 btn btn-sm btn-primary' onClick={this.submit}>
						Add
					</button>
				</form>
			</div>
		)
	}
}

export default ActivitateNouaCmp

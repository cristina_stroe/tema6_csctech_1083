import React from "react"
import axios from "axios"
import { Link } from "react-router-dom"
class ActivitateCmp extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			cod: ""
		}

		this.inscriere = this.inscriere.bind(this)
		this.esteInscris = this.esteInscris.bind(this)
		this.codeChange = this.codeChange.bind(this)
	}

	codeChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	esteInscris() {
		return this.props.legaturi.find(legatura => legatura.idActivitate === this.props.activitate.id)
	}

	async inscriere() {
		if (parseInt(this.state.cod) === this.props.activitate.id) {
			await axios.post("http://18.191.219.161:8080/api/legaturaActivitateUtilizator", {
				idActivitate: this.props.activitate.id,
				idUtilizator: this.props.utilizator.id
			})

			this.props.refresh()
		} else {
			this.setState({ cod: "" })
		}
	}
	render() {
		return (
			<div className='card ml-2' style={{ width: 15 + "%" }}>
				<div className='card-body mr-2'>
					<div className='card-title'>{this.props.activitate.numeActivitate}</div>
					<div className='card-text'>
						Data: {new Date(this.props.activitate.data).toLocaleDateString()}
						<br></br>
						Durata: {this.props.activitate.durata}
					</div>
					{this.esteInscris() || this.props.utilizator.profesor ? (
						<Link to={"/feedback/" + this.props.activitate.id}>
							<button className='btn btn-sm btn-primary'>Detalii</button>
						</Link>
					) : (
						<div>
							<input
								value={this.state.cod}
								placeholder='cod'
								className='form-control mb-2 mt-2'
								type='number'
								name='cod'
								onChange={this.codeChange}
							></input>
							<button className='btn btn-sm btn-primary' onClick={this.inscriere}>
								Inscriere
							</button>
						</div>
					)}
				</div>
			</div>
		)
	}
}

export default ActivitateCmp

import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import axios from "axios"
import ActivitateNouaCmp from "./ActivitateNouaCmp"
import FeedbackCmp from "./FeedbackCmp"
import PrincipalCmp from "./PrincipalCmp"
import LoginCmp from "./LoginCmp"

class App extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			utilizator: {}
		}

		this.submit = this.submit.bind(this)
	}

	async submit(username, password) {
		return await axios.get(`http://18.191.219.161:8080/api/utilizator/${username}`).then(data => {
			if (data.data.password === password) {
				this.setState({ utilizator: data.data })
				return true
			}
			return false
		})
	}
	render() {
		return (
			<div>
				<Router>
					<Switch>
						<Route
							exact
							path={"/activitateNoua"}
							render={({ history }) => (
								<ActivitateNouaCmp
									history={history}
									utilizator={this.state.utilizator}
								></ActivitateNouaCmp>
							)}
						/>
						<Route
							exact
							path={"/"}
							render={({ history }) => <LoginCmp history={history} submit={this.submit}></LoginCmp>}
						/>
						<Route
							exact
							path={"/principal"}
							render={({ history }) => (
								<PrincipalCmp history={history} utilizator={this.state.utilizator}></PrincipalCmp>
							)}
						/>
						<Route
							exact
							path={"/feedback/:id"}
							render={props => (
								<FeedbackCmp
									history={props.history}
									utilizator={this.state.utilizator}
									{...props}
								></FeedbackCmp>
							)}
						/>
						/>
					</Switch>
				</Router>
			</div>
		)
	}
}

export default App

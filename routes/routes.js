const express = require("express")
const router = express.Router()
const { getActivitate, createActivitate, getToateActivitati } = require("../controller/activitate")
const { getUtilizator, createUtilizator } = require("../controller/utilizator.js")
const {
	getLegaturaActivitateUtilizator,
	createLegaturaActivitateUtilizator
} = require("../controller/legaturaActivitateUtilizator")
const {
	getLegaturaActivitateEmoticon,
	createLegaturaActivitateEmoticon
} = require("../controller/legaturaActivitateEmoticon")
const { getEmoticoane } = require("../controller/emoticon")

router.get("/activitate/:id", getActivitate)
router.get("/activitate", getToateActivitati)
router.get("/legaturaActivitateUtilizator/:userId", getLegaturaActivitateUtilizator)
router.get("/utilizator/:username", getUtilizator)
router.post("/activitate", createActivitate)
router.get("/emoticons", getEmoticoane)
router.get("/legaturaActivitateEmoticon/:idActivitate", getLegaturaActivitateEmoticon)
router.post("/utilizator", createUtilizator)
router.post("/legaturaActivitateUtilizator", createLegaturaActivitateUtilizator)
router.post("/legaturaActivitateEmoticon", createLegaturaActivitateEmoticon)

module.exports = router

const legaturaActivitateUtilizatorService = require("./../service/legaturaActivitateUtilizator")

const getLegaturaActivitateUtilizator = async (req, res, next) => {
	try {
		const legaturiActivitateUtilizator = await legaturaActivitateUtilizatorService.get(
			req.params.userId
		)
		res.status(200).send(legaturiActivitateUtilizator)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createLegaturaActivitateUtilizator = async (req, res, next) => {
	const legaturaActivitateUtilizator = req.body

	if (legaturaActivitateUtilizator.idActivitate && legaturaActivitateUtilizator.idUtilizator) {
		let result = await legaturaActivitateUtilizatorService.create(legaturaActivitateUtilizator)
		if (result) {
			res.status(201).send({
				message: "Legatura a fost creata"
			})
		} else {
			res.status(400).send({
				message: "Nu se poate inscrie"
			})
		}
	} else {
		res.status(400).send({
			message: "Date gresite"
		})
	}
}

module.exports = {
	getLegaturaActivitateUtilizator,
	createLegaturaActivitateUtilizator
}

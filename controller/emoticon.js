const emoticoaneService = require("./../service/emoticon")

const getEmoticoane = async (req, res, next) => {
	try {
		const emoticoane = await emoticoaneService.get()
		res.status(200).send(emoticoane)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

module.exports = {
	getEmoticoane
}

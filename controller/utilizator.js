const utilizatorService = require("./../service/utilizator")

const getUtilizator = async (req, res, next) => {
	try {
		const utilizator = await utilizatorService.get(req.params.username)
		res.status(200).send(utilizator)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createUtilizator = async (req, res, next) => {
	const utilizator = req.body

	if (
		utilizator.username &&
		utilizator.password &&
		utilizator.nume &&
		utilizator.prenume &&
		utilizator.profesor
	) {
		await utilizatorService.create(utilizator)
		res.status(201).send({
			message: "Utilizatorul a fost creat"
		})
	} else {
		res.status(400).send({
			message: "Date gresite"
		})
	}
}

module.exports = {
	getUtilizator,
	createUtilizator
}

const legaturaActivitateEmoticonService = require("./../service/legaturaActivitateEmoticon")

const getLegaturaActivitateEmoticon = async (req, res, next) => {
	try {
		const legaturaActivitateEmoticon = await legaturaActivitateEmoticonService.get(
			req.params.idActivitate
		)
		res.status(200).send(legaturaActivitateEmoticon)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createLegaturaActivitateEmoticon = async (req, res, next) => {
	const legaturaActivitateEmoticon = req.body

	if (legaturaActivitateEmoticon.idActivitate && legaturaActivitateEmoticon.idEmoticon) {
		await legaturaActivitateEmoticonService.create(legaturaActivitateEmoticon)
		res.status(201).send({
			message: "Legatura a fost creata"
		})
	} else {
		res.status(400).send({
			message: "Date gresite"
		})
	}
}

module.exports = {
	getLegaturaActivitateEmoticon,
	createLegaturaActivitateEmoticon
}

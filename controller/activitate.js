const activitateService = require("./../service/activitate")

const getActivitate = async (req, res, next) => {
	try {
		const activitate = await activitateService.get(req.params.id)
		res.status(200).send(activitate)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const getToateActivitati = async (req, res, next) => {
	try {
		const activitati = await activitateService.getAll()
		res.status(200).send(activitati)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createActivitate = async (req, res, next) => {
	const activitate = req.body

	if (activitate.numeActivitate && activitate.data && activitate.durata) {
		await activitateService.create(activitate)
		res.status(201).send({
			message: "Activitatea a fost creata"
		})
	} else {
		res.status(400).send({
			message: "Date gresite"
		})
	}
}

module.exports = {
	getActivitate,
	getToateActivitati,
	createActivitate
}

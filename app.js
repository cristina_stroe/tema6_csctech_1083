const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")

const PORT = process.env.PORT || 8080
const router = require("./routes/routes")
const app = express()

app.use(bodyParser.json())
app.use(cors())
app.use("/api", router)

app.listen(PORT, '0.0.0.0', () => {
	console.log(`Started at ${PORT}...`)
})

const { LegaturaActivitateEmoticon } = require("../models/legaturaActivitateEmoticon")

const legaturaActivitateEmoticon = {
	get: async idActivitate => {
		return await LegaturaActivitateEmoticon.findAll({
			where: {
				idActivitate: idActivitate
			}
		})
	},
	create: async legaturaActivitateEmoticon => {
		try {
			return await LegaturaActivitateEmoticon.create(legaturaActivitateEmoticon)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = legaturaActivitateEmoticon

const { Emoticon } = require("../models/emoticon")

const emoticon = {
	get: async () => {
		return await Emoticon.findAll()
	}
}

module.exports = emoticon

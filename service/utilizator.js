const { Utilizator } = require("../models/utilizator")

const utilizator = {
	get: async username => {
		return await Utilizator.findOne({
			where: {
				username: username
			}
		})
	},
	create: async utilizator => {
		try {
			return await Utilizator.create()
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = utilizator

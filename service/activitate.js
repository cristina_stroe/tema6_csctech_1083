const { Activitate } = require("../models/activitate")

const activitate = {
	get: async id => {
		return await Activitate.findOne({
			where: {
				id: parseInt(id)
			}
		})
	},

	getAll: async () => {
		return await Activitate.findAll({
			order: [["data", "DESC"]]
		})
	},
	create: async activitate => {
		try {
			return await Activitate.create(activitate)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = activitate

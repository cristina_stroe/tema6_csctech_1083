const { LegaturaActivitateUtilizator } = require("../models/legaturaActivitateUtilizator")
const { Activitate } = require("../models/activitate")

const legaturaActivitateUtilizator = {
	get: async userId => {
		return await LegaturaActivitateUtilizator.findAll({
			where: {
				idUtilizator: parseInt(userId)
			}
		})
	},
	create: async legaturaActivitateUtilizator => {
		try {
			return await Activitate.findOne({
				where: {
					id: legaturaActivitateUtilizator.idActivitate
				}
			}).then(async activitate => {
				if (activitate) {
					let now = new Date()
					let activityDate = new Date(activitate.data.setHours(activitate.data.getHours()))
					let data = new Date(activityDate.getTime() + activitate.durata * 60000)
					if (now < new Date(activityDate) || now > data) {
						return false
					} else {
						return await LegaturaActivitateUtilizator.create(legaturaActivitateUtilizator)
					}
				}
			})
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = legaturaActivitateUtilizator
